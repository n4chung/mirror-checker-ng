.DEFAULT_GOAL := build-n-run

run:
	./bin/mc2

build:
	go build -o bin/mc2 -ldflags "\
		-X 'mc2/config.BuildVersion=$$(git rev-parse --abbrev-ref HEAD)' \
		-X 'mc2/config.BuildUser=$$(id -u -n)' \
		-X 'mc2/config.BuildTime=$$(date)' \
		-X 'mc2/config.BuildGOOS=$$(go env GOOS)' \
		-X 'mc2/config.BuildARCH=$$(go env GOARCH)' \
		-s -w"

docker-build:
	docker build -t mc2 .

docker-run:
	docker run --rm -p 4200:4200 mc2

test:
	go test -v ./...
