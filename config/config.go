package config

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"os"
	"time"

	"github.com/Nathan13888/mirror-checker2/v2/checkers"
	"github.com/rs/zerolog/log"
)

var MirrorBaseURL = "http://mirror.csclub.uwaterloo.ca/" // TODO: must start with https://

var loggingTimeFormat = time.RFC3339

type MirrorData map[string]checkers.ProjectProperties

func LoadFromFile(path string) error {
	raw_data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	var data MirrorData

	err = json.Unmarshal(raw_data, &data)
	if err != nil {
		return err
	}

	// Access the parsed data
	for proj, prop := range data {
		log.Info().Str("project", proj).Msg("Enabled Project.")

		sp, exists := checkers.SupportedProjects[NormalizeName(proj)]
		if !exists {
			log.Warn().Str("project", proj).Msg("Project not supported.")
			continue
		}

		sp.Properties = prop

		log.Debug().
			Str("distribution", proj).
			// Time("out_of_sync_since", prop.OOSSince).
			Int64("out_of_sync_interval", prop.OOSInterval).
			Str("csc", prop.CSC).
			Str("upstream", prop.Upstream).
			Str("file", prop.File).
			Msg("Loaded project config.")
	}

	return nil
}

func ReadEnabledFromStdin() error {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)

	for scanner.Scan() {
		token := scanner.Text()
		_, err := checkers.LoadProject(token)
		if err != nil {
			return err
		}
	}

	return scanner.Err()
}
