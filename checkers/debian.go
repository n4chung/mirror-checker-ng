package checkers

var DebianProject Project = Project{
	Name:          "debian",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("debian"),
	},
}
