package checkers

import (
	"time"

	"github.com/rs/zerolog/log"
)

////////////////////////////////////////////////////////////////////////////////

var DefaultProjectProperties = ProjectProperties{
	OOSSince:    time.Now(),
	OOSInterval: 3600,
	CSC:         "unknown",
	Upstream:    "unknown",
	File:        "unknown",
}

var DefaultCheckerResult CheckerResult = CheckerResult{
	ProjectName: "unknown",
	CheckerName: "unknown",
	Status:      CHECKER_PROGRESS,
}

////////////////////////////////////////////////////////////////////////////////

// All "projects" which have been implemented.
var SupportedProjects map[string]*Project = map[string]*Project{
	"debian": &DebianProject,
}

func LoadDefaultProjects() {
	// Load all projects that's implemented

	log.Info().Msg("Loading Default Projects.")

	LoadProject("debian")

}

func GetDefaultChecker(name string) *ProjectChecker {
	return &ProjectChecker{
		CheckProject: func() CheckerResult {
			// TODO: implement
			return DefaultCheckerResult
		},
	}
}
