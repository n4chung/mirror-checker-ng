package checkers

import (
	"errors"
	"time"

	"github.com/rs/zerolog/log"
)

// "Projects" which have valid configs.
var EnabledProjects = make(map[string]*Project)

// TODO: refactor all errors into variables

////////////////////////////////////////////////////////////////////////////////
// Project, Project Properties, Project Checker

type Project struct {
	Name          string
	Properties    ProjectProperties
	NumOfCheckers int
	Checkers      []*ProjectChecker
}

type ProjectProperties struct {
	OOSSince    time.Time //`json:"out_of_sync_since"`
	OOSInterval int64     `json:"out_of_sync_interval"` // in seconds
	CSC         string    `json:"csc"`
	Upstream    string    `json:"upstream"`
	File        string    `json:"file"`
}

type ProjectChecker struct {
	CheckProject func() CheckerResult
}

// //////////////////////////////////////////////////////////////////////////////
// Checker Status and Checker Result
type CheckerStatus string

var CHECKER_SUCCESS CheckerStatus = "success"
var CHECKER_PROGRESS CheckerStatus = "progress"
var CHECKER_ERROR CheckerStatus = "error"
var CHECKER_FAIL CheckerStatus = "fail"

type CheckerResult struct {
	ProjectName string        `json:"project_name"`
	CheckerName string        `json:"checker_name"`
	Time        time.Time     `json:"time"`
	Status      CheckerStatus `json:"status"`
	Error       error         `json:"error"`
}

type CheckerResultCallback func(CheckerResult)

////////////////////////////////////////////////////////////////////////////////

// Given a project, all checks are initiated.
func (p Project) RunChecksAsync(callback CheckerResultCallback) error {
	checks := p.Checkers
	n := len(checks)

	// TODO: assert other checkers?
	// assert if the number of checkers match the expected number of checkers
	if n != p.NumOfCheckers {
		return errors.New("Number of checkers does not match the expected number of checkers.")
	}

	log.Info().Msgf("Running %d checks for project %s", n, p.Name)
	for i := 0; i < n; i++ {
		check := checks[i]
		log.Info().Str("project", p.Name).Msgf("Running check %d", i)

		check.RunCheckAsync(callback)
	}
	return nil
}

// Given a check, run the check asynchronously and call the callback function.
func (c ProjectChecker) RunCheckAsync(callback CheckerResultCallback) error {
	go func() {
		res := c.CheckProject()
		callback(res)
	}()
	return nil
}

////////////////////////////////////////////////////////////////////////////////

// Looks up a project by name, and returns only if the project is enabled.
func GetProject(name string) (*Project, error) {
	res, exists := EnabledProjects[name]
	if !exists {
		return res, errors.New("Requested project not found.")
	}

	return res, nil
}

// Loads a project by name, and returns if the project is found.
func LoadProject(name string) (*Project, error) {
	res, exists := SupportedProjects[name]
	if !exists {
		return res, errors.New("Requested project not found.")
	}
	if res.Properties == DefaultProjectProperties {
		log.Debug().Str("csc", res.Properties.CSC).
			Msgf("Requested project %s has default properties.", name)
		return res, errors.New("Requested project has invalid properties. Please check the project config.")
	}

	log.Debug().Msgf("Loading Project %s", name)
	EnabledProjects[name] = res
	return res, nil
}
