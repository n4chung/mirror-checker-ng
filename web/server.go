package web

import "github.com/gofiber/fiber/v2"

func StartServer() error {
	app := fiber.New()

	// TODO: authentication middleware? is it needed?

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hi! Why are you here? :)")
	})
	app.Get("/health", getHealth)
	app.Get("/healthz", getHealth)
	app.Get("/alive", getHealth)

	app.Get("/status", getStatus)

	app.Get("/api/project/:proj/all", getProjectAll)
	app.Get("/api/project/:proj/status", getProjectStatus)
	// TODO: initiate re-checks?

	app.Listen(":4200") // TODO: custom port and address

	return nil
}

func getProjectAll(c *fiber.Ctx) error {
	return c.SendStatus(200)
}

func getProjectStatus(c *fiber.Ctx) error {
	return c.SendStatus(200)
}

func getHealth(c *fiber.Ctx) error {
	return c.SendStatus(200)
}

func getStatus(c *fiber.Ctx) error {
	// TODO: implement
	return c.SendStatus(200)
}
