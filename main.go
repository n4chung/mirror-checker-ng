package main

import (
	"os"

	"github.com/Nathan13888/mirror-checker2/v2/checkers"
	"github.com/Nathan13888/mirror-checker2/v2/config"
	"github.com/Nathan13888/mirror-checker2/v2/web"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
)

var configPath string

func loadConfig() {
	path := config.GetAbsPath(configPath)
	// TODO: check if file exists

	log.Info().Str("path", path).Msg("Loading config file.")
	err := config.LoadFromFile(path)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to load config file.")
	}
}

func main() {
	config.SetupLogger(true) // TODO: flag for debug mode

	// Start CLI
	// TODO: documentation - https://cli.urfave.org/v2/examples/full-api-example/
	app := &cli.App{
		Name:                 "CSC Mirror Checker 2",
		Usage:                "sees if the mirror is up!",
		Version:              config.BuildVersion,
		EnableBashCompletion: true,
		// https://cli.urfave.org/v2/examples/combining-short-options/
		// TODO: flags for config file (mirrors.json), defaults to mirrors.json
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "config",
				Aliases:     []string{"c"},
				Usage:       "path to config file",
				Destination: &configPath,
				Value:       "data/mirrors.json",
				EnvVars:     []string{"MC_CONFIG_FILE"},
			},
		},
		Commands: []*cli.Command{
			{
				Name:    "daemon",
				Aliases: []string{"s", "d", "serve", "web", "server"},
				Usage:   "starts web API",
				Action: func(cCtx *cli.Context) error {
					// TODO: flags for port, listen address
					// TODO: flag --config or --stdin-config

					// TODO: enable all projects by default
					// checkers.LoadDefaultProjects()

					loadConfig()

					return web.StartServer()
				},
			},
			{
				Name:    "check",
				Aliases: []string{"c"},
				Usage:   "checks particular mirror (once)",
				Action: func(cCtx *cli.Context) error {
					checkers.LoadDefaultProjects()
					loadConfig()

					// verify all projects are enabled
					// TODO: ???

					// attempt to look up and check all projects
					projects := cCtx.Args().Slice()
					if len(projects) == 0 {
						log.Fatal().Msg("No projects specified.")
					}

					log.Debug().Msgf("Checking all specified projects.")

					for _, arg := range projects {
						log.Info().Msgf("Pulling project information for '%s'.", arg)
						proj, err := checkers.LoadProject(arg)
						if err != nil {
							log.Fatal().Err(err).
								Str("project", arg).
								Msg("Failed to load project.")
							return err
						}

						err = proj.RunChecksAsync(func(res checkers.CheckerResult) {
							if res.Error != nil {
								log.Error().Err(res.Error).
									Time("time", res.Time).
									Msgf("Failed check %s for project %s", res.CheckerName, res.ProjectName)
							} else {
								log.Info().
									Time("time", res.Time).
									Str("status", string(res.Status)).
									Msgf("Completed check %s for project %s", res.CheckerName, res.ProjectName)
							}
						})
						if err != nil {
							log.Fatal().Err(err).Msg("Failed to check project.")
						}
					}

					return nil
				},
				// TODO: auto complete available mirrors, https://cli.urfave.org/v2/examples/combining-short-options/
				// BashComplete: func(cCtx *cli.Context) {
				// 	// This will complete if no args are passed
				// 	if cCtx.NArg() > 0 {
				// 		return
				// 	}
				// 	for _, t := range config.Mirrors {
				// 		log.Println(t)
				// 	}
				// },
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal().Err(err).Msg("An error occurred.")
	}
}
